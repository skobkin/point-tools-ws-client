package main

import (
	"fmt"
	"log"
	"net/url"
	"os"
	"os/signal"
	"time"

	"bitbucket.org/skobkin/point-tools-ws-client/point_ws"
	"encoding/json"
	"github.com/gorilla/websocket"
	"github.com/iwanbk/gobeanstalk"
	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	// Build data
	buildDate    string
	// CLI params
	wsScheme = kingpin.Flag("ws-scheme", "WebSocket URL wsScheme (ws/wss)").Default("wss").String()
	wsHost   = kingpin.Flag("ws-host", "Point.im WebSocket wsHost").Default("point.im").String()
	wsPath   = kingpin.Flag("ws-path", "WebSocket path (for example: \"/ws\"").Default("/ws").String()
	wsToken  = kingpin.Flag("ws-token", "Authentication token received from /api/login").Envar("WS_TOKEN").Required().String()
	bsHost   = kingpin.Flag("bs-host", "Beanstalkd host").Default("localhost").Envar("BS_HOST").String()
	bsPort	 = kingpin.Flag("bs-port", "Beanstalkd port").Default("11300").Envar("BS_PORT").String()
	bsTube   = kingpin.Flag("bs-tube", "Beanstalkd tube name").Default("point-websocket-updates").Envar("BS_TUBE").String()
	bsDisabled = kingpin.Flag("bs-disable", "Disable Beanstalkd queue").Bool()
)

func main() {
	log.Printf("Point Tools WS listener built %s", buildDate)

	kingpin.Parse()

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)

	u := url.URL{Scheme: *wsScheme, Host: *wsHost, Path: *wsPath}
	logMessage("!", fmt.Sprintf("connecting to WebSocket at %s", u.String()))

	c, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		log.Fatal("dial:", err)
	}
	defer c.Close()

	authString := fmt.Sprintf("Authorization: %v", *wsToken)
	logMessage(">", authString)

	err = c.WriteMessage(websocket.TextMessage, []byte(authString))
	if err != nil {
		log.Println("<< ", err)
		return
	}

	// Connecting to Beanstalkd
	var bsQueue *gobeanstalk.Conn
	if !*bsDisabled {
		bsConnString := *bsHost + ":" + *bsPort
		logMessage("!", fmt.Sprintf("Connecting to Beanstalkd at %s", bsConnString))
		bsQueue, err = gobeanstalk.Dial(bsConnString)
		if err != nil {
			log.Fatal("Beanstalkd error:", err)
		}

		bsQueue.Use(*bsTube)
	}

	done := make(chan struct{})

	go func() {
		defer c.Close()
		defer close(done)

		var messagesCount int64 = 0;

		for {
			_, message, err := c.ReadMessage()
			if err != nil {
				log.Println("WS read error:", err)

				if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway) {
					log.Fatal("WS critical error. Closing.")
				}

				return
			}

			messagesCount++;
			messageText := string(message)

			logMessage("<", messageText)

			// First received message must be an auth
			if messagesCount == 1 {
				var auth point_ws.Auth

				err := json.Unmarshal(message, &auth)
				if err != nil {
					log.Fatal("Error while decoding Auth object:", err)
				}
				if auth.Login != nil {
					logMessage("!", fmt.Sprintf("Successfully authenticated as %s", *auth.Login))

					// Skipping message processing
					continue
				} else {
					log.Fatal("Auth data is nil. Exiting...")
				}
			}

			if "ping" != messageText {
				if !*bsDisabled {
					jobId, err := bsQueue.Put(message, 0, 0, 30*time.Second)
					if err != nil {
						log.Fatalln("Beanstalkd PUT failed:", err)
					}
					log.Println("bs #", jobId)
				}
			}
		}
	}()

	for {
		select {
		case <-interrupt:
			log.Println("interrupt")
			// To cleanly close a connection, a client should send a close
			// frame and wait for the server to close the connection.
			err := c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			if err != nil {
				log.Println("write close:", err)
				return
			}
			select {
			case <-done:
			case <-time.After(time.Second):
			}
			c.Close()
			return
		}
	}
}

func logMessage(prefix string, message string) {
	log.Printf(
		"%s %s",
		prefix,
		message)
}