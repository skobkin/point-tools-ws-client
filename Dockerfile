FROM golang:1-alpine as builder

WORKDIR /go/src/bitbucket.org/skobkin/point-tools-ws-client

ENV GOPATH=/go

# Installing build deps
RUN apk update && apk add \
    bash \
    git \
    ca-certificates

RUN go get -u github.com/golang/dep/cmd/dep

COPY point-ws-listener.go /go/src/bitbucket.org/skobkin/point-tools-ws-client/
COPY point_ws /go/src/bitbucket.org/skobkin/point-tools-ws-client/point_ws
COPY Gopkg.toml /go/src/bitbucket.org/skobkin/point-tools-ws-client/
COPY Gopkg.lock /go/src/bitbucket.org/skobkin/point-tools-ws-client/

RUN dep ensure
RUN go install -ldflags "-s -w -X main.buildDate=`date -u +%Y-%m-%dT%H:%M:%SZ`"

# Second stage (clean production build)
FROM alpine:latest

WORKDIR /go/bin

# Copying service binary
COPY --from=builder /go/bin/point-tools-ws-client .
# Copy CA certs (for Telegram API)
COPY --from=builder /etc/ssl/certs /etc/ssl/certs

ENV WS_TOKEN="" \
    BS_HOST="localhost" \
    BS_PORT=11300 \
    BS_TUBE="point-websocket-updates"

ENTRYPOINT ["/go/bin/point-tools-ws-client"]
