package point_ws

// This object is returned by Point.im WebSocket after successfull authentication
type Auth struct {
	Login *string `json:"login"`
}
